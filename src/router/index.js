import Vue from 'vue'
import Router from 'vue-router'
import Flight from '../components/Flight.vue'
import Home from '../components/Home.vue'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home
		},
		{
			path: '/flight',
			name: 'Flight',
			component: Flight
		}
	]
})
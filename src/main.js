import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate'
import VueRouter from 'vue-router';
import router from './router'

Vue.use(VueRouter);
Vue.use(Vuelidate)
Vue.use(require('vue-moment'));

Vue.config.productionTip = false

new Vue({
	router,
	render: h => h(App),
}).$mount('#app')
